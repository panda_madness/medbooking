<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');

Route::post('/help', 'HomeController@help')->name('help-form');

/* Booking Routes */

Route::group(['prefix' => 'booking'], function () {
    
    Route::get('/', 'BookingController@index')->name('booking.index');
    
    Route::get('/search/hospitals', 'BookingController@searchHospitals')->name('booking.search.hospitals');
    Route::get('/search/services', 'BookingController@searchServices')->name('booking.search.services');
    Route::get('/search/bundles', 'BookingController@searchBundles')->name('booking.search.bundles');
    
});

Route::get('admin/services/get-service-categories', 'ServiceController@getServiceCategories');

Route::group(['prefix' => 'categories'], function() {
    Route::get('/service_categories/{category}', 'CategoryController@getServiceCategories');
});

/* Hospital details routes */

Route::get('hospitals/{hospital}/{bundle}', 'HospitalController@bundle')->name('hospitals.bundle');

/* Profile routes */

Route::group([
    'prefix' => 'profile',
    'middleware' => 'auth'
], function () {
    Route::get('/', 'ProfileController@index')->name('profile.index');
});

/* Shopping Cart Routes  */

Route::group([
    'prefix' => 'cart',
    'middleware' => 'auth'
], function () {
    
    Route::get('/', 'CartController@index')->name('cart.index');
    
    Route::get('/add/{type}/{id}', 'CartController@store')->name('cart.add');
    
    Route::get('/delete/{type}/{id}', 'CartController@destroy')->name('cart.delete');

    Route::get('/send', 'OrdersController@create')->name('cart.send');
    
});

/* Orders routes */

Route::group([
    'prefix' => 'orders',
    'middleware' => 'auth'
], function () {
    Route::post('/create', 'OrdersController@create')->name('order.create');
});

Route::group([], function () {
    
    Route::get('/faq', 'PagesController@faq')->name('pages.faq');
    
    Route::get('/articles', 'PagesController@articles')->name('pages.articles');
    
    Route::get('/articles/{id}', 'PagesController@article')->name('pages.articles.show');
    
    Route::get('/news', 'PagesController@news')->name('pages.news');
    
});

Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth', 'admin']
], function() {
   
    Route::get('/', 'AdminController@index')->name('admin.index');
    
    Route::resource('/news', 'NewsController');
    Route::resource('/articles', 'ArticleController');
    Route::resource('/faq', 'FaqController');
    Route::resource('/hospitals', 'HospitalController');
    Route::resource('/categories', 'CategoryController');
    Route::get('/services/get_by_category', 'ServiceController@getByCategory');
    Route::resource('/services', 'ServiceController');
    Route::resource('/bundles', 'BundleController');
    Route::post('/bundles/{id}/duplicate', 'BundleController@duplicate')->name('bundles.duplicate');
    Route::resource('/users', 'UserController');
    Route::resource('/cities', 'CityController');
    Route::resource('/orders', 'OrdersController', ['except' => ['create', 'store']]);
    
});

Route::get('/hospitals/get-by-city', 'HospitalController@getByCityId')->name('hospitals.get-by-city');
Route::get('/services/get-by-service-category', 'ServiceController@getByServiceCategoryId')->name('services.get-by-service-category');
