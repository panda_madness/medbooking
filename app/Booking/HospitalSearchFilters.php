<?php

namespace App\Booking;

use Illuminate\Database\Query\Builder;

class HospitalSearchFilters extends QueryFilter
{
    public function hospital_category($id)
    {
        $this->builder->where('category_id', $id);
    }
    
    public function hospital_name($name = null)
    {
        $this->builder->where('name', 'like', "%${name}%");
    }
    
    public function hospital_city($id)
    {
        $this->builder->where('city_id', $id);
    }
}