<?php

namespace App\Booking;

use Illuminate\Database\Query\Builder;

class ServiceSearchFilters extends QueryFilter
{
    public function service_category($id)
    {
        $this->builder->where('category_id', $id);
    }
    
    public function name_ru($name = '')
    {
        $this->builder->where('name_ru', 'like',"%${name}%");
    }
    
    public function price_from($value = 0)
    {
        $this->builder->where('price', '>=', $value);
    }
    
    public function price_to($value = 1000000)
    {
        $this->builder->where('price', '<=', $value);
    }
}