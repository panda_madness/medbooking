<?php

namespace App\Booking;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class QueryFilter
{
    /**
     * @var Request
     */
    protected $request;
    
    /**
     * @var Builder
     */
    protected $builder;
    
    /**
     * QueryFilter constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    /**
     * @param Builder $builder
     */
    public function apply(Builder $builder)
    {
        $this->builder = $builder;
        
        foreach ($this->filters() as $name => $value) {
            if(method_exists($this, $name)) {
                if(trim($value)) {
                    $this->$name($value);
                } else {
                    $this->$name();
                }
            }
        }
        
        return $this->builder;
    }
    
    /**
     * @return array
     */
    public function filters()
    {
        return $this->request->all();
    }
}