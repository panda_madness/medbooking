<?php

namespace App\Booking;

use Illuminate\Database\Query\Builder;

class BundleSearchFilters extends QueryFilter
{
    public function service_category($id)
    {
        $this->builder->where('category_id', $id);
    }
}