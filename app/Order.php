<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Service;
use App\Bundle;

class Order extends Model
{
    protected $casts = [
        'contents' => 'array'
    ];

    public static function createOrder($bundle_id)
    {
        $order = new Order();

        $order->status = 'pending';
        $order->user_id = request()->user()->id;
        $order->contents = ['bundle_id' => $bundle_id];

        $order->save();
    }
    
    public function user()
    {
        $this->belongsTo(User::class);
    }
    
    public function getContentsAttribute($value)
    {
        $contents = json_decode($value);
        $return = [];
        
        $return['services'] = Service::whereIn('id', $contents->services);
        $return['bundles'] = Service::whereIn('id', $contents->bundles);
        
        return $return;
    }
}
