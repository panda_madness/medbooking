<?php

namespace App\Http\Requests;

use App\Hospital;
use Illuminate\Foundation\Http\FormRequest;

class HospitalSearchForm extends FormRequest implements SearchFormInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hospital_category' => 'required'
        ];
    }
    
    public function process()
    {
        $input = $this->all();
    
        $hospitals = Hospital::with([
            'services' => function($query) use($input) {
                if(isset($input['service_price_to'])) {
                    $query->where('price', '<=', $input['service_price_to']);
                }
    
                if(isset($input['service_price_from'])) {
                    $query->where('price', '>=', $input['service_price_from']);
                }
    
                if(isset($input['service_name'])) {
                    $query->where('name_ru', 'like', "%${input['service_name']}%");
                }
    
                if(isset($input['service_category'])) {
                    $query->where('service_category_id', '=', $input['service_category']);
                }
            },
            'bundles' => function($query) use($input) {
                if(isset($input['bundle_price_to'])) {
                    $query->where('price_male', '<=', $input['bundle_price_to'])
                        ->orWhere('price_female', '<=', $input['bundle_price_to']);
                }
    
                if(isset($input['bundle_price_from'])) {
                    $query->where('price_male', '>=', $input['bundle_price_from'])
                        ->orWhere('price_female', '>=', $input['bundle_price_from']);
                }
    
                if(isset($input['bundle_name'])) {
                    $query->where('name_ru', 'like', "%${input['bundle_name']}%");
                }
            }
        ]);
        
        if(isset($input['hospital_name'])) {
            $hospitals->where('name', 'like', "%${input['hospital_name']}%");
        }
    
        if(isset($input['hospital_city'])) {
            $hospitals->where('city_id', '=', $input['hospital_city']);
        }
        
        return $hospitals->get();
    }
}
