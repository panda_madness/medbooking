<?php

namespace App\Http\Requests;

use Illuminate\Database\Eloquent\Collection;

interface SearchFormInterface {
    
    /**
     *
     * @return Collection
     */
    public function process();
}