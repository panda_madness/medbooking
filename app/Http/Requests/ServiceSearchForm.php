<?php

namespace App\Http\Requests;

use App\Service;
use Illuminate\Foundation\Http\FormRequest;

class ServiceSearchForm extends FormRequest implements SearchFormInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
    
    public function process()
    {
        $filters = resolve('App\Booking\ServiceSearchFilters');
    
        return Service::filter($filters);
    }
}
