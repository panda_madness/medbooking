<?php

namespace App\Http\Controllers;

use App\Hospital;
use Illuminate\Http\Request;
use App\Service;
use App\Bundle;

class BundleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bundles = Bundle::all();
        return view('admin.bundles.index', ['bundles' => $bundles]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $input = $request->all();
        
        $hospital = Hospital::find($input['hospital_id']);
    
        $services = Service::where('category_id', $hospital['category_id'])->get();
        
        return view('admin.bundles.create', [
            'services' => $services,
            'hospital' => $hospital
        ]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $bundle = new Bundle();
    
        $bundle->name = $input['name'];
        $bundle->description = $input['description'];
        $bundle->goal = $input['goal'];
        $bundle->notes = $input['notes'];
        $bundle->price_male = $input['price_male'];
        $bundle->price_female = $input['price_female'];
        $bundle->class = $input['class'];
        $bundle->hospital()->associate($input['hospital_id']);
    
        $bundle->save();
    
        $bundle->services()->sync($input['services']);
        
        return redirect()->route('hospitals.edit', ['id' => $input['hospital_id']]);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $input = $request->all();
        
        $bundle = Bundle::with('services')->where('id', '=', $id)->first();
    
        $hospital = Hospital::find($input['hospital_id']);
    
        $services = Service::where('category_id', $hospital['category_id'])->get();
        
        return view('admin.bundles.edit', [
            'bundle' => $bundle,
            'services' => $services,
            'hospital' => $hospital
        ]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        
        $bundle = Bundle::find($id);
        
        $bundle->name = $input['name'];
        $bundle->description = $input['description'];
        $bundle->notes = $input['notes'];
        $bundle->price_male = $input['price_male'];
        $bundle->price_female = $input['price_female'];
        $bundle->class = $input['class'];
        
        $bundle->save();
        
        $bundle->services()->sync($input['services']);
        
        return redirect()->route('hospitals.edit', ['id' => $input['hospital_id']]);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Bundle::destroy($id);
        return back();
    }
    
    /**
     * Duplicate the specified bundle in the database
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function duplicate($id)
    {
        $old_bundle = Bundle::find($id);
        
        $service_ids = [];
        
        foreach ($old_bundle->services as $service) {
            $service_ids[] = $service['id'];
        }
        
        $new_bundle = $old_bundle->replicate();
        
        $new_bundle->save();
        
        $new_bundle->services()->sync($service_ids);
        
        return back();
    }
}
