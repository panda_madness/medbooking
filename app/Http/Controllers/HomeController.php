<?php

namespace App\Http\Controllers;

use App\Category;
use App\Mail\HelpRequested;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return view('home', ['categories' => $categories]);
    }

    public function help(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        Mail::to(env('MAIL_FROM_ADDRESS'))->send(new HelpRequested(
            $request->get('name'),
            $request->get('email'),
            $request->get('message'))
        );

        return view('help');
    }
}
