<?php

namespace App\Http\Controllers;

use App\City;
use App\Hospital;
use App\Http\Requests\BundleSearchForm;
use App\Http\Requests\HospitalSearchForm;
use App\Http\Requests\ServiceSearchForm;
use Illuminate\Http\Request;
use App\Repositories\Booking;
use App\Service;
use App\Bundle;
use App\Category;

class BookingController extends Controller
{
    /**
     * Display the main search page
     * @param Request $request
     */
    public function index(Request $request)
    {
        $categories = Category::all();
        $cities = City::all();
        
        $surgery_category = Category::with(['service_categories'])
            ->where('key', 'surgery')
            ->first();
    
        $plastic_surgery = Category::with(['service_categories'])
            ->where('key', 'plastic-surgery')
            ->first();
    
        $dentistry = Category::with(['service_categories'])
            ->where('key', 'dentistry')
            ->first();
        
        return view('booking.index', [
            'categories' => $categories,
            'cities' => $cities,
            'surgery_category' => $surgery_category,
            'plastic_surgery' => $plastic_surgery,
            'dentistry' => $dentistry,
        ]);
    }
    
    public function searchHospitals(HospitalSearchForm $form)
    {
        $hospitals = $form->process();
        
        return view('booking.search', ['hospitals' => $hospitals]);
    }
    
    public function searchBundles(Request $request)
    {
        $this->validate($request, [
            'bundle_category' => 'numeric',
            'price_from' => 'numeric',
            'price_to' => 'numeric',
        ]);

        $categories = Category::all();

        $classes = [
            [
                "id" => "1",
                "name" => "standard",
                "title" => "Стандартный"
            ],
            [
                "id" => "2",
                "name" => "detailed",
                "title" => "Детальный"
            ],
            [
                "id" => "3",
                "name" => "specialized",
                "title" => "Специализированный"
            ],
            [
                "id" => "4",
                "name" => "premuim",
                "title" => "Премиум"
            ]
        ];

        $bundles = Bundle::whereHas('hospital', function($query) use ($request, $categories) {
                $query->where('category_id', $request->get('bundle_category', $categories->first()->id));
            }
        )->with('hospital');

        if ($request->has('bundle_name')) {
            $name = $request->get('bundle_name');
            $bundles->where('name', 'like', "%${name}%");
        }

        if ($request->has('bundle_class')) {
            $class = $request->get('bundle_class');
            $bundles->where('class', 'like', "%${class}%");
        }

        if ($request->has('price_from') && $request->has('price_to')) {
            $bundles->whereBetween('price_male', [
                $request->get('price_from'), $request->get('price_to')
            ]);
        }

        $bundles = $bundles->paginate(24);
        
        return view('booking.search.bundles-result', compact('bundles', 'categories', 'classes'));
    }
    
    public function searchServices(Request $request)
    {
        $this->validate($request, [
            'category' => 'required|numeric',
            'service_category' => 'required|numeric',
            'service' => 'required|numeric',
        ]);
        
        $hospitals = Hospital::whereHas('services', function ($query) use($request) {
            $query->where('id', $request->get('service'));
        })->with([
            'services' => function($query) use($request) {
                $query->where('id', $request->get('service'));
            }
        ])->where('category_id', $request->get('category'))->get();
        
        return view('booking.search.services-result', [
            'hospitals' => $hospitals
        ]);
    }
    
    /**
     * Displays single bundle information
     */
    public function bundle($id)
    {
        return view('booking.bundle', ['bundle' => Bundle::find($id)]);
    }
    
    /**
     * Displays single service information
     */
    public function service($id)
    {
        return view('booking.service', ['service' => Service::find($id)]);
    }
}
