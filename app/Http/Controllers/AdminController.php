<?php

namespace App\Http\Controllers;

use App\Repositories\ServiceImporter;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        
    }
    
    public function services()
    {
        return view('admin.services.index');
    }
    
    public function showImportServices()
    {
        return view('admin.services.import');
    }
    
    public function importServices(Request $request, ServiceImporter $importer)
    {
        $this->validate($request, [
            'services' => 'required|file'
        ]);
    
        if($importer->import($request)) {
            return redirect()->back()->with('message', 'Сервисы загружены');
        } else {
            return redirect()->back()->with('message', 'Произошла ошибка');
        }
    }
    
    public function hospitals()
    {
        return view('admin.hospitals.index');
    }
}
