<?php

namespace App\Http\Controllers;

use App\Bundle;
use App\Mail\BundleOrdered;
use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\Mail;

class OrdersController extends Controller
{
    public function create(Request $request)
    {
        
        Order::createOrder($request->get('bundle_id'));

        $user = $request->user();

        $bundle = Bundle::find($request->get('bundle_id'));

        Mail::to(env('MAIL_USERNAME'))->send(new BundleOrdered($user, $bundle));
        
        return view('booking.success');
    }
}
