<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\CartStorageInterface;

/**
 * Class CartController
 * Handles CRUD operations on the shopping cart
 * @package App\Http\Controllers
 */
class CartController extends Controller
{
    protected $cart;
    
    public function __construct(CartStorageInterface $cart)
    {
        $this->cart = $cart;
    }
    
    public function index()
    {
        $cart = $this->cart->get();
        
        return view('cart', ['cart' => $cart]);
    }
    
    public function store($type, $id)
    {
        $this->cart->add($type, $id);
        
        return redirect('/cart');
    }
    
    public function update()
    {
        
    }
    
    public function destroy($type, $id)
    {
        $this->cart->delete($type, $id);
    
        return redirect('/cart');
    }
}