<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\News;
use App\Article;
use App\Faq;

class PagesController extends Controller
{
    public function faq()
    {
        return view('faq', ['faqs' => Faq::all()]);
    }
    
    public function articles()
    {
        return view('articles', ['articles' => Article::all()]);
    }
    
    public function article($id)
    {
        return view('article', ['article' => Article::find($id)]);
    }
    
    public function news()
    {
        return view('news', ['news' => News::all()]);
    }
}
