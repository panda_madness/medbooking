<?php

namespace App\Http\Controllers;

use App\Category;
use App\Repositories\ServiceImporter;
use App\ServiceCategory;
use Illuminate\Http\Request;
use App\Service;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_by = $request->get('sort_by') ?? 'id';
        
        $direction = $request->get('direction') ?? 'asc';
        
        $categories = Category::all();
    
    
        $category_id = $request->get('category_id') ?? false;
        
        $service_category_id = $request->get('service_category_id') ?? false;
    
        $service_categories = $categories->first();
        
        $services = Service::orderBy($sort_by, $direction);
        
        if($category_id) {
            $services->where('category_id', $category_id);
            $service_categories = $categories->where('id', $category_id)->first();
        }

        if($service_category_id) {
            $services->where('service_category_id', $service_category_id);
        }
        
        $services = $services->get();
        
        $service_categories = $service_categories->service_categories;
        
        return view('admin.services.index', [
            'services' => $services,
            'categories' => $categories,
            'service_categories' => $service_categories
        ]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        
        $service_categories = $categories->first()->service_categories;
        
        return view('admin.services.create', [
            'categories' => $categories,
            'service_categories' => $service_categories
        ]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $service = new Service();
    
        $service->name_ru = $input['name_ru'];
        $service->description = $input['description'];
        $service->type = $input['type'] === 'null' ? null : $input['type'];
        $service->category_id = $input['category_id'];
        $service->service_category_id = $input['service_category_id'];
        $service->gender = $input['gender'];
        
        $service->save();
        
        
    
        return redirect()->route('services.index');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);
    
        return view('admin.services.edit', ['service' => $service]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
    
        $service = Service::find($id);
        
        $service->name_ru = $input['name_ru'];
        $service->description = $input['description'];
    
        $service->type = $input['type'] === 'null' ? null : $input['type'];
        $service->gender = $input['gender'];
        
        $service->save();
        
        return redirect()->route('services.index');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Service::destroy($id);
        
        return redirect()->route('services.index');
    }
    
    public function getServiceCategories(Request $request)
    {
        $category_id = $request->get('category_id');
        
        $service_categories = ServiceCategory::where('category_id', $category_id)->get();
        
        return view('admin.services.service-categories', [
            'service_categories' => $service_categories
        ]);
    }
    
    public function getByServiceCategoryId(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|numeric'
        ]);
        
        $category_id = $request->get('id');
    
        $services = Service::where('service_category_id', $category_id)->get()->toArray();
        
        return response()->json($services);
    }
}
