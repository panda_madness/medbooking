<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\City;
use App\ServiceCategory;
use App\Service;
use App\Bundle;
use App\Hospital;
use Illuminate\Support\Facades\Validator;

class HospitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hospitals = Hospital::orderBy('id', 'desc')->get();
    
        return view('admin.hospitals.index', ['hospitals' => $hospitals]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bundles = Bundle::all();
        $services = Service::where('type', '!=', 'default')->get();
        $cities = City::all();
        $categories = Category::all();
        
        return view('admin.hospitals.create', [
            'services' => $services,
            'bundles' => $bundles,
            'categories' => $categories,
            'cities' => $cities
        ]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'file|image',
            'gallery.*' => 'file|image'
        ]);
        
        $input = $request->all();
        
        $hospital = new Hospital();
        
        $hospital->name = $input['name'];
        $hospital->description = $input['description'];
        $hospital->image = null;
        $hospital->city_id = $input['city_id'];
        $hospital->category_id = $input['category_id'];
        $hospital->notes = $input['notes'];
        $hospital->visa_support = $input['visa_support'];
        $hospital->additional_transfer = $input['additional_transfer'];
        $hospital->international = !empty($input['international']);
        if($request->has('image')) {
            $hospital->image = $request->file('image')->store('hospitals', 'public');
        }

        $gallery = [];

        if($request->has('gallery')) {
            if(is_array($request->file('gallery'))) {
                foreach ($request->file('gallery') as $file) {
                    $gallery[] = $file->store('hospitals', 'public');
                }
            } else {
                $gallery[] = $request->file('gallery')->store('hospitals', 'public');
            }
        }

        $hospital->gallery = $gallery;

        $hospital->save();
        
        return redirect()->route('hospitals.edit', ['id' => $hospital['id']]);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hospital = Hospital::with('services')
            ->where('id', '=', $id)
            ->first();
        
        return response()->json($hospital->toArray());
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hospital = Hospital::find($id);
        $bundles = Bundle::all();
        $services = Service::where('category_id', '=', $hospital->category_id)
            ->where('type', '!=', 'default')
            ->orWhere('type', '=', null)
            ->get();
        $cities = City::all();
        $categories = Category::all();
        
        return view('admin.hospitals.edit', [
            'hospital' => $hospital,
            'services' => $services,
            'bundles' => $bundles,
            'categories' => $categories,
            'cities' => $cities
        ]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image' => 'file|image',
            'gallery.*' => 'file|image'
        ]);

        $hospital = Hospital::find($id);
        $services = [];
        
        $input = $request->all();
    
        $hospital->name = $input['name'];
        $hospital->description = $input['description'];
        $hospital->city_id = $input['city_id'];
        $hospital->notes = $input['notes'];
        $hospital->visa_support = $input['visa_support'];
        $hospital->additional_transfer = $input['additional_transfer'];
        $hospital->international = !empty($input['international']);

        if($request->file('image')) {
            $hospital->image = $request->file('image')->store('hospitals', 'public');
        }


        if ($request->file('gallery')) {
            $gallery = [];

            if(is_array($request->file('gallery'))) {
                foreach ($request->file('gallery') as $file) {
                    $gallery[] = $file->store('hospitals', 'public');
                }
            } else {
                $gallery[] = $request->file('gallery')->store('hospitals', 'public');
            }

            $hospital->gallery = $gallery;
        }

        $hospital->save();
        
        if(isset($input['services'])) {
            foreach ($input['services'] as $service) {
                $service_id = explode(':', $service)[0];
                $service_price = explode(':', $service)[1];
        
                $services[$service_id] = ['price' => $service_price];
            }
    
            $hospital->services()->sync($services);
        }
        
        return back();
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Hospital::destroy($id);
        
        return back();
    }
    
    public function getByCityId(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|numeric'
        ]);
        
        $hospitals = Hospital::where('city_id', $request->get('id'))->get()->toArray();
        
        return response()->json($hospitals);
    }

    public function bundle(Hospital $hospital, Bundle $bundle)
    {
        return view('hospitals.bundle', compact('hospital', 'bundle'));
    }
}
