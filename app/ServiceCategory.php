<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    public function service()
    {
        return $this->hasMany(Service::class, 'service_category_id');
    }
    
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
