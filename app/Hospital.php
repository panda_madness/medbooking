<?php

namespace App;

use App\Booking\QueryFilter;
use Illuminate\Database\Eloquent\Model;
use App\Service;
use App\Bundle;
use App\City;

class Hospital extends Model
{
    protected $casts = [
        'gallery' => 'array'
    ];
    
    protected $fillable = ['name', 'description', 'city_id', 'category_id'];
    
    public function services() {
        return $this->belongsToMany(Service::class)->withPivot('price');
    }
    
    public function bundles() {
        return $this->hasMany(Bundle::class);
    }
    
    public function city() {
        return $this->belongsTo(City::class);
    }
    
    public function category() {
        return $this->belongsTo(Category::class);
    }
    
    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }
}
