<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function service_categories()
    {
        return $this->hasMany(ServiceCategory::class, 'category_id');
    }
}
