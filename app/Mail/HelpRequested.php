<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class HelpRequested extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $user_message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $message)
    {
        $this->name = $name;
        $this->email = $email;
        $this->user_message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('help@inmeding.com')
            ->subject('Заявка о помощи')
            ->view('mail.help-form')
            ->with([
                'name' => $this->name,
                'email' => $this->email,
                'user_message' => $this->user_message
            ]);
    }
}
