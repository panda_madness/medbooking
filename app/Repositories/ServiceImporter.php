<?php

namespace App\Repositories;

use App\Category;
use Illuminate\Http\Request;
use App\Service;

class ServiceImporter {
    
    public function import(Request $request)
    {
        $file = fopen($request->file('services')->getPathname(), 'r');
    
        while(!feof($file)) {
            $csv[] = fgetcsv($file);
        }
    
        $header = array_shift($csv);
    
        array_walk($csv, function(&$row, $key, $header) {
            $row = array_combine($header, $row);
        }, $header);
        
        foreach ($csv as $row) {
            $service = new Service();
            
            $service->name_ru = $row['name_ru'];
            $service->name_kr = $row['name_kr'];
            $service->name_en = $row['name_en'];
            
            $service->description = $row['description'];
            
            $service->category()->associate(
                Category::where('name', '=', $row['category'])->first()
            );
            
            $service->save();
        }
        
        return true;
    }
    
}