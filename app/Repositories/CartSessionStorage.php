<?php

namespace App\Repositories;

use App\Interfaces\CartStorageInterface;

use App\Service;
use App\Bundle;

class CartSessionStorage implements CartStorageInterface {
    
    public function get()
    {
        $cart = session('cart');
        
        $services = isset($cart['services']) ? Service::whereIn('id', $cart['services'])->get() : [];
        $bundles = isset($cart['bundles']) ? Bundle::whereIn('id', $cart['bundles'])->get() : [];
        
        return [
            'services' => $services,
            'bundles' => $bundles
        ];
    }
    
    public function add($type, $id)
    {
        $cart = session('cart');
        
        if(!isset($cart[$type])) {
            $cart[$type] = [];
        }
        
        if (!in_array($id, $cart[$type])) {
            $cart[$type][] = $id;
            session(['cart' => $cart]);
            return true;
        }
        
        return false;
    }
    
    public function delete($type, $id)
    {
        $cart = session('cart');
    
        if(($key = array_search($id, $cart[$type])) !== false) {
            unset($cart[$type][$key]);
            session(['cart' => $cart]);
            return true;
        }
    
        return false;
    }
    
    public function update($type, $id)
    {
        // TODO: Implement update() method.
    }

    public function empty()
    {
        session(['cart' => []]);

        return true;
    }
}