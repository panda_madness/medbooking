<?php

namespace App\Repositories;

use App\Service;
use App\Hospital;
use App\Bundle;


/**
 * Class Booking
 * Searches through and fetches results based on input
 * TODO: This needs heavy refactoring, currently not pretty
 * @package App\Repositories
 */
class Booking {
    
    public static function getResults(array $input) {
        
        switch ($input['search-category']) {
            case 'bundles':
                return self::getBundles($input);
                break;
            case 'examinations':
                return self::getServices($input);
                break;
            case 'dentistry':
                return self::getServices($input, 'dentistry');
                break;
            case 'plastic-surgery':
                return self::getServices($input, 'plastic-surgery');
                break;
            case 'treatment':
                return self::getServices($input, 'treatment');
                break;
        }
        
        return false;
        
    }
    
    private static function getBundles(array $input) {
        $name = $input['service-name'] ?: '';
        
        return Bundle::where('name', 'like', "%${name}%")->get();
    }
    
    private static function getServices(array $input, $category = null) {
        $name = $input['service-name'] ?: '';
        
        return Service::whereHas('category', function ($query) use($category) {
            $query->where('name', 'like', "%${category}%");
        })
            ->where('name', 'like', "%${name}%")
            ->get();
    }
    
}