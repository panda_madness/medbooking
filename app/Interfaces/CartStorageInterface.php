<?php

namespace App\Interfaces;

interface CartStorageInterface {
    
    public function get();
    public function add($type, $id);
    public function delete($type, $id);
    public function update($type, $id);
    public function empty();
    
}

