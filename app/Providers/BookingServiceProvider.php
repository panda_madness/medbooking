<?php

namespace App\Providers;

use App\Booking\QueryFilter;
use App\Booking\BundleSearchFilters;
use App\Booking\HospitalSearchFilters;
use App\Booking\ServiceSearchFilters;
use App\Bundle;
use App\Hospital;
use App\Service;
use Illuminate\Support\ServiceProvider;

class BookingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
//        $this->app->when(Hospital::class)
//            ->needs(QueryFilter::class)
//            ->give(new HospitalSearchFilters(request()));
//
//        $this->app->when(Service::class)
//            ->needs(QueryFilter::class)
//            ->give(new ServiceSearchFilters(request()));
//
//        $this->app->when(Bundle::class)
//            ->needs(QueryFilter::class)
//            ->give(new BundleSearchFilters(request()));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
