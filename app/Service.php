<?php

namespace App;

use App\Booking\QueryFilter;
use Illuminate\Database\Eloquent\Model;

use App\Category;

class Service extends Model
{
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    
    public function service_category()
    {
        return $this->belongsTo(ServiceCategory::class, 'service_category_id');
    }
    
    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }
}
