<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Service;
use App\Hospital;

class Bundle extends Model
{
    public function services() {
        return $this->belongsToMany(Service::class);
    }
    
    public function hospital() {
        return $this->belongsTo(Hospital::class);
    }
}
