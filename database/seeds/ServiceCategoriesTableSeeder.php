<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\ServiceCategory;

class ServiceCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = Category::where('key', '=', 'hospital')->first()->id;
        
        ServiceCategory::create([
            'name' => 'Стоматология',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Хирургия',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Пластическая хирургия',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'УЗИ',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'КТ',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'ПЭТ КТ',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'МРТ',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Онко-обследования',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Аллергия',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Разное',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Женские обследования',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Генетические',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Мужские обследования',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Диагностика сердечно-сосудистой системы',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Гинекология',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Обследование',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Анализы крови',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Рентген',
            'category_id' => $id
        ]);
    
        ServiceCategory::create([
            'name' => 'Лабораторные исследования',
            'category_id' => $id
        ]);
    }
}
