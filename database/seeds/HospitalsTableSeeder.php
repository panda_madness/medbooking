<?php

use Illuminate\Database\Seeder;

class HospitalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $services = App\Service::all();
        
        factory(App\Hospital::class, 40)->create()->each(function ($h) use($services) {
            
            $service_ids = [];
            $bundle_ids = [];
            
            foreach ($services->random(random_int(4, 8))->pluck('id') as $id) {
                $service_ids[$id] = ['price' => random_int(200, 400)];
            }
            
            $h->services()->sync($service_ids);
        });
    }
}
