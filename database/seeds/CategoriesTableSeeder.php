<?php

use Illuminate\Database\Seeder;

use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'key' => 'hospital',
            'name' => 'Госпиталь',
            'icon' => 'categories/hospital.svg'
        ]);
        
        Category::create([
            'key' => 'dentistry',
            'name' => 'Стоматология',
            'icon' => 'categories/dentistry.svg'
        ]);
    
        Category::create([
            'key' => 'wellness',
            'name' => 'Велнесс центр',
            'icon' => 'categories/wellness.svg'
        ]);
    
        Category::create([
            'key' => 'plastic-surgery',
            'name' => 'Пластическая хирургия',
            'icon' => 'categories/plastic-surgery.svg'
        ]);
    
        Category::create([
            'key' => 'surgery',
            'name' => 'Хирургия',
            'icon' => 'categories/surgery.svg'
        ]);
    
        Category::create([
            'key' => 'eastern-medicine',
            'name' => 'Восточная медицина',
            'icon' => 'categories/eastern-medicine.svg'
        ]);
    
        Category::create([
            'key' => 'medical-centre',
            'name' => 'Медицинский центр',
            'icon' => 'categories/medical-centre.svg'
        ]);
    
        Category::create([
            'key' => 'clinic',
            'name' => 'Больница',
            'icon' => 'categories/clinic.svg'
        ]);
    }
}
