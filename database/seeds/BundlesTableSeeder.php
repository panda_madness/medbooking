<?php

use Illuminate\Database\Seeder;

class BundlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = App\Service::all();
        factory(App\Bundle::class, 15)->create()->each(function ($b) use($services) {
            $b->services()->saveMany($services->random(rand(3, 8)));
        });
    }
}
