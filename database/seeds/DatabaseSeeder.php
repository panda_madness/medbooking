<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ServiceCategoriesTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(HospitalsTableSeeder::class);
        $this->call(BundlesTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(ArticlesTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(RelationshipSeeder::class);
    }
}
