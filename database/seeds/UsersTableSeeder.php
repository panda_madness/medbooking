<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 20)->create();
        
        App\User::create([
            'name' => 'Margulan',
            'email' => 'thecigaroman@gmail.com',
            'role' => 'admin',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
        ]);
    }
}
