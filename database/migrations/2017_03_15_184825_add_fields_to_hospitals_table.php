<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToHospitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hospitals', function (Blueprint $table) {
            $table->boolean('international')->nullable();
            $table->string('visa_support')->nullable();
            $table->text('additional_transfer')->nullable();
            $table->text('notes')->nullable();
            $table->text('gallery')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hospitals', function (Blueprint $table) {
            $table->dropColumn('international');
            $table->dropColumn('visa_support');
            $table->dropColumn('additional_transfer');
            $table->dropColumn('notes');
        });
    }
}
