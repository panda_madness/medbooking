<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'role' => 'customer',
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Order::class, function (Faker\Generator $faker) {
    return [
        'status' => (rand() % 2 === 0) ? 'pending':'confirmed',
        'user_id' => function() {
            return App\User::all()->random()->id;
        },
        'contents' => function() {
    
            $sids = [];
            $bids = [];
        
            $services = App\Service::all()->random(rand(2, 6))->toArray();
            
            foreach ($services as $item) {
                $sids[] = $item['id'];
            }
    
            $bundles = App\Bundle::all()->random(rand(2, 6))->toArray();
    
            foreach ($bundles as $item) {
                $bids[] = $item['id'];
            }
            
            return json_encode([
                'bundles' => $bids,
                'services' => $sids
            ]);
        }
    ];
});

$factory->define(App\Service::class, function (Faker\Generator $faker) {
    
    return [
        'name_en' => $faker->words(3, true),
        'name_ru' => $faker->words(3, true),
        'name_kr' => $faker->words(3, true),
        'description' => $faker->paragraphs(3, true),
        'image' => $faker->image(),
        'category_id' => function() {
            return App\Category::all()->random()->id;
        },
        'type' => function() {
            switch(random_int(1, 3)) {
                case 1:
                    return 'popular';
                    break;
                case 2:
                    return 'default';
                    break;
                case 3:
                    return null;
                    break;
            }
            
            return null;
        },
        'service_category_id' => function() {
            return App\ServiceCategory::all()->random()->id;
        }
    ];
});

$factory->define(App\Bundle::class, function (Faker\Generator $faker) {
    
    return [
        'name' => $faker->words(3, true),
        'description' => $faker->paragraphs(3, true),
        'goal' => $faker->paragraphs(1, true),
        'notes' => $faker->paragraphs(2, true),
        'price_male' => random_int(1000, 2000),
        'price_female' => random_int(1000, 2000),
        'image' => $faker->image(),
        'hospital_id' => App\Hospital::all()->random()->id
    ];
});

$factory->define(App\City::class, function (Faker\Generator $faker) {
    
    return [
        'name' => $faker->words(1, true)
    ];
});

$factory->define(App\Hospital::class, function (Faker\Generator $faker) {
    
    return [
        'name' => $faker->words(1, true),
        'description' => $faker->paragraphs(3, true),
        'image' => $faker->image(),
        'city_id' => function() {
            return App\City::all()->random()->id;
        },
        'category_id' => function() {
            return App\Category::all()->random()->id;
        }
    ];
});

$factory->define(App\Faq::class, function (Faker\Generator $faker) {
    
    return [
        'question' => $faker->words(4, true),
        'answer' => $faker->words(4, true)
    ];
});

$factory->define(App\Article::class, function (Faker\Generator $faker) {
    
    return [
        'title' => $faker->words(3, true),
        'contents' => $faker->paragraphs(2, true)
    ];
});

$factory->define(App\News::class, function (Faker\Generator $faker) {
    
    return [
        'title' => $faker->words(3, true),
        'contents' => $faker->paragraphs(2, true)
    ];
});
