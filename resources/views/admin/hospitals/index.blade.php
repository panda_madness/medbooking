@extends('layouts.app')

@section('content')
    <section>
        <h1>Hospitals</h1>

        <div class="form-group">
            <a href="{{ route('hospitals.create') }}" class="btn btn-primary">Создать</a>
        </div>

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <td>Название</td>
                <td>Описание</td>
                <td>Город</td>
                <td>Категория</td>
                <td>Редактировать</td>
                <td>Удалить</td>
            </tr>
            </thead>

            <tbody>
            @foreach($hospitals as $hospital)
                <tr>
                    <td>{{ $hospital['name'] }}</td>
                    <td>{{ str_limit($hospital['description'], 50) }}</td>
                    <td>{{ $hospital->city['name'] }}</td>
                    <td>{{ $hospital->category['name'] }}</td>
                    <td><a class="btn btn-primary" href="{{ route('hospitals.edit', ['id' => $hospital['id']]) }}">Редактировать</a></td>
                    <td>
                        <form action="{{ route('hospitals.destroy', ['id' => $hospital['id']]) }}" method="POST">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <input type="submit" class="btn btn-danger" value="Удалить">
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </section>
@endsection