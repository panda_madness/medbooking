@extends('layouts.app')

@section('content')
    <section>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#info" aria-controls="info" role="tab" data-toggle="tab">Информация о госпитале</a>
            </li>

            <li role="presentation">
                <a href="#bundles" aria-controls="bundles" role="tab" data-toggle="tab">Бандлы</a>
            </li>

            <li role="presentation">
                <a href="#services" aria-controls="services" role="tab" data-toggle="tab">Услуги</a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="info">
                <h1>Edit Hospital</h1>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="{{ route('hospitals.update', ['id' => $hospital['id']]) }}" method="POST" id="hospital-edit" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="hospital-name">Название</label>
                        <input type="text" name="name" id="hospital-name" class="form-control" value="{{ $hospital['name'] }}">
                    </div>

                    <div class="form-group">
                        <label for="city">Город</label>
                        <select name="city_id" id="city" class="form-control">
                            @foreach($cities as $city)
                                <option @if($hospital['city_id'] == $city['id']) selected @endif value="{{ $city['id'] }}">{{ $city['name'] }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="category">Категория</label>
                        <input type="text" class="form-control" value="{{ $hospital->category->name }}" disabled>
                    </div>

                    <div class="form-group">
                        <label>Визовая поддержка</label>
                        <input type="text" name="visa_support" class="form-control" value="{{ $hospital['visa_support'] }}">
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="international" value="true" @if($hospital['international']) checked @endif>
                            <span>Международный отдел</span>
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="image">Главная фотка</label>
                        <input type="file" name="image" id="image" accept="image/png,image/jpeg,image/gif">
                        <img src="{{ asset('storage/' . $hospital->image) }}" class="img-responsive" alt="">
                    </div>

                    <div class="form-group">
                        <label for="gallery">Фотографии для галереи</label>
                        <input type="file" name="gallery[]" id="gallery" accept="image/png,image/jpeg,image/gif" multiple>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            @if(count($hospital->gallery))
                                @foreach($hospital->gallery as $item)
                                    <div class="col-md-3">
                                        <img src="{{ asset('storage/' . $item) }}" alt="" class="img-responsive">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="hospital-description">Описание</label>
                        <textarea rows="10" name="description" id="hospital-description" class="form-control html-editor">{{ $hospital['description'] }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="additional_transfer">Дополнительный трансфер</label>
                        <textarea rows="10" name="additional_transfer" id="additional_transfer" class="form-control html-editor">{{ $hospital['additional_transfer'] }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="hospital-notes">Примечания</label>
                        <textarea rows="10" name="notes" id="hospital-notes" class="form-control html-editor">{{ $hospital['notes'] }}</textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>

            <div role="tabpanel" class="tab-pane" id="bundles">
                <h2>Бандлы</h2>
                <div class="form-group">
                    <form action="{{ route('bundles.create') }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <input type="hidden" name="hospital_id" value="{{ $hospital['id'] }}">
                        <button type="submit" class="btn btn-primary">Создать бандл</button>
                    </form>
                </div>
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <td>Название</td>
                        <td>Редактировать</td>
                        <td>Дублировать</td>
                        <td>Удалить</td>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($hospital->bundles as $bundle)
                        <tr>
                            <td>{{ $bundle['name'] }}</td>
                            <td>
                                <form action="{{ route('bundles.edit', ['id' => $bundle['id']]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('GET') }}
                                    <input type="hidden" name="hospital_id" value="{{ $hospital['id'] }}">
                                    <input type="submit" class="btn btn-primary" value="Редактировать" />
                                </form>
                            </td>

                            <td>
                                <form action="{{ route('bundles.duplicate', ['id' => $bundle['id']]) }}" method="POST">
                                    {{ method_field('POST') }}
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-primary" value="Дублировать">
                                </form>
                            </td>

                            <td>
                                <form action="{{ route('bundles.destroy', ['id' => $bundle['id']]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <input type="submit" class="btn btn-danger" value="Удалить" />
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div role="tabpanel" class="tab-pane" id="services">
                <h2>Услуги</h2>
                @foreach($hospital->category->service_categories as $service_category)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ $service_category['name'] }}</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                @foreach($services as $service)
                                    @if($service['service_category_id'] == $service_category['id'])
                                        <li class="list-group-item">
                                            <label>{{ $service['name_ru'] }}
                                                <input
                                                        type="text"
                                                        class="form-control service-data"
                                                        data-service-id="{{ $service['id'] }}"
                                                        value="{{ $hospital->services()->where('id', $service['id'])->first()->pivot->price or '' }}"
                                                >
                                            </label>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection