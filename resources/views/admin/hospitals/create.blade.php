@extends('layouts.app')

@section('content')
    <section>
        <h1>Create Hospital</h1>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('hospitals.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="form-group">
                <label for="hospital-name">Название</label>
                <input type="text" name="name" id="hospital-name" class="form-control">
            </div>

            <div class="form-group">
                <label for="city">Город</label>
                <select name="city_id" id="city" class="form-control">
                    @foreach($cities as $city)
                        <option value="{{ $city['id'] }}">{{ $city['name'] }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="category">Категория</label>
                <select name="category_id" id="category" class="form-control">
                    @foreach($categories as $category)
                        <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="visa_support">Визовая поддержка</label>
                <input type="text" name="visa_support" id="visa_support" class="form-control">
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="international" value="true">
                    <span>Международный отдел</span>
                </label>
            </div>

            <div class="form-group">
                <label for="image">Главная фотка</label>
                <input type="file" name="image" id="image" accept="image/png,image/jpeg,image/gif">
            </div>

            <div class="form-group">
                <label for="gallery">Фотографии для галереи</label>
                <input type="file" name="gallery[]" id="gallery" accept="image/png,image/jpeg,image/gif" multiple>
            </div>

            <div class="form-group">
                <label for="hospital-description">Описание</label>
                <textarea rows="10" name="description" id="hospital-description" class="form-control html-editor"></textarea>
            </div>

            <div class="form-group">
                <label for="additional_transfer">Дополнительный трансфер</label>
                <textarea rows="10" name="additional_transfer" id="additional_transfer" class="form-control html-editor"></textarea>
            </div>

            <div class="form-group">
                <label for="hospital-notes">Примечания</label>
                <textarea rows="10" name="notes" id="hospital-notes" class="form-control html-editor"></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
    </section>
@endsection