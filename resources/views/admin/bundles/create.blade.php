@extends('layouts.app')

@section('content')
    <section>
        <h1>Create Bundle</h1>

        <form action="{{ route('bundles.store') }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('POST') }}

            <div class="form-group">
                <label for="hospital-name">Госпиталь</label>
                <input type="text" id="hospital-name" class="form-control" value="{{ $hospital['name'] }}" disabled>
                <input type="hidden" name="hospital_id" value="{{ $hospital['id'] }}">
            </div>

            <div class="form-group">
                <label for="bundle-name">Название</label>
                <input type="text" name="name" id="bundle-name" class="form-control">
            </div>

            <div class="form-group">
                <label for="bundle_class">Название</label>
                <select name="class" id="bundle_class">
                    <option value="standard">Стандартный</option>
                    <option value="detailed">Детальный</option>
                    <option value="specialized">Специализированный</option>
                    <option value="premuim">Премиум</option>
                </select>
            </div>

            <div class="form-group">
                <label for="bundle-goal">Цель</label>
                <input type="text" name="goal" id="bundle-goal" class="form-control">
            </div>

            <div class="form-group">
                <label for="bundle-description">Описание</label>
                <textarea rows="10" name="description" id="bundle-description" class="form-control html-editor"></textarea>
            </div>

            <div class="form-group">
                <label for="bundle-notes">Примечания</label>
                <textarea rows="10" name="notes" id="bundle-notes" class="form-control html-editor"></textarea>
            </div>

            <div class="row">
                <h2>Услуги</h2>
                <div class="form-group col-md-4">
                    <h3>Дефолтные</h3>
                    <button id="js-uncheck-default-services" class="btn btn-default">Снять дефолтные чекбоксы</button>
                    @foreach($services as $service)
                        @if($service['type'] == 'default')
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="js-default-services" name="services[]" value="{{ $service['id'] }}" checked>
                                    {{ $service['name_ru'] }}
                                </label>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="form-group col-md-4">
                    <h3>Популярные</h3>
                    @foreach($services as $service)
                        @if($service['type'] == 'popular')
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="services[]" value="{{ $service['id'] }}">
                                    {{ $service['name_ru'] }}
                                </label>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="form-group col-md-4">
                    <h3>Прочие</h3>
                    @foreach($services as $service)
                        @if($service['type'] == null)
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="services[]" value="{{ $service['id'] }}">
                                    {{ $service['name_ru'] }}
                                </label>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>

            <div class="form-group">
                <label for="">Цена (муж)</label>
                <input type="text" class="form-control" name="price_male">
            </div>

            <div class="form-group">
                <label for="">Цена (жен)</label>
                <input type="text" class="form-control" name="price_female">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
    </section>
@endsection