@extends('layouts.app')

@section('content')
    <section>
        <h1>Edit Bundle</h1>

        <form action="{{ route('bundles.update', ['id' => $bundle['id']]) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <input type="hidden" name="hospital_id" value="{{ $hospital['id'] }}">

            <div class="form-group">
                <label for="hospital-name">Госпиталь</label>
                <input type="text" id="hospital-name" class="form-control" value="{{ $hospital['name'] }}" disabled>
                <input type="hidden" name="hospital_id" value="{{ $hospital['id'] }}">
            </div>

            <div class="form-group">
                <label for="bundle-name">Название</label>
                <input type="text" name="name" id="bundle-name" class="form-control" value="{{ $bundle['name'] }}">
            </div>

            <div class="form-group">
                <label for="bundle_class">Класс</label>
                <select name="class" id="bundle_class" class="form-control">
                    <option value="standard" {{ $bundle->class == 'standard' ? 'selected':'' }}>Стандартный</option>
                    <option value="detailed" {{ $bundle->class == 'detailed' ? 'selected':'' }}>Детальный</option>
                    <option value="specialized" {{ $bundle->class == 'specialized' ? 'selected':'' }}>Специализированный</option>
                    <option value="premuim" {{ $bundle->class == 'specific' ? 'selected':'' }}>Премиум</option>
                </select>
            </div>

            <div class="form-group">
                <label for="bundle-goal">Цель</label>
                <input type="text" name="goal" id="bundle-goal" class="form-control" value="{{ $bundle['goal'] }}">
            </div>

            <div class="form-group">
                <label for="bundle-description">Описание</label>
                <textarea rows="10" name="description" id="bundle-description" class="form-control html-editor">{{ $bundle['description'] }}</textarea>
            </div>

            <div class="form-group">
                <label for="bundle-notes">Примечания</label>
                <textarea rows="10" name="notes" id="bundle-notes" class="form-control html-editor">{{ $bundle['notes'] }}</textarea>
            </div>

            <div class="row">
                <h2>Услуги</h2>
                <div class="form-group col-md-4">
                    <h3>Дефолтные</h3>
                    @foreach($services as $service)
                        @if($service['type'] == 'default')
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="services[]" value="{{ $service['id'] }}"
                                           @if($bundle->services->contains('id', $service['id']))
                                           checked
                                            @endif
                                    >
                                    {{ $service['name_ru'] }}
                                </label>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="form-group col-md-4">
                    <h3>Популярные</h3>
                    @foreach($services as $service)
                        @if($service['type'] == 'popular')
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="services[]" value="{{ $service['id'] }}"
                                           @if($bundle->services->contains('id', $service['id']))
                                           checked
                                            @endif
                                    >
                                    {{ $service['name_ru'] }}
                                </label>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="form-group col-md-4">
                    <h3>Прочие</h3>
                    @foreach($services as $service)
                        @if($service['type'] == null)
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="services[]" value="{{ $service['id'] }}"
                                           @if($bundle->services->contains('id', $service['id']))
                                           checked
                                            @endif
                                    >
                                    {{ $service['name_ru'] }}
                                </label>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>

            <div class="form-group">
                <label for="">Цена (муж)</label>
                <input type="text" class="form-control" name="price_male" value="{{ $bundle['price_male'] }}">
            </div>

            <div class="form-group">
                <label for="">Цена (жен)</label>
                <input type="text" class="form-control" name="price_female" value="{{ $bundle['price_female'] }}">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
    </section>
@endsection