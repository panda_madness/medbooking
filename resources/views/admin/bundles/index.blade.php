@extends('layouts.app')

@section('content')
    <section>
        <h1>Bundles</h1>

        <div class="form-group">
            <a href="{{ route('bundles.create') }}" class="btn btn-primary">Создать</a>
        </div>

        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td>Название</td>
                    <td>Описание</td>
                    <td>Редактировать</td>
                    <td>Дублировать</td>
                    <td>Удалить</td>
                </tr>
            </thead>

            <tbody>
                @foreach($bundles as $bundle)
                    <tr>
                        <td>{{ $bundle['name'] }}</td>
                        <td>{{ $bundle['description'] }}</td>
                        <td><a class="btn btn-primary" href="{{ route('bundles.edit', ['id' => $bundle['id'], 'hospital_id' => $bundle['hospital_id']]) }}">Редактировать</a></td>
                        <td>
                            <form action="{{ route('bundles.duplicate', ['id' => $bundle['id']]) }}" method="POST">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <input type="submit" class="btn btn-primary" value="Дублировать">
                            </form>
                        </td>
                        <td>
                            <form action="{{ route('bundles.destroy', ['id' => $bundle['id']]) }}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <input type="submit" class="btn btn-danger" value="Удалить">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </section>
@endsection