@extends('layouts.app')

@section('content')
    <section>
        <h1>Services</h1>

        <div class="form-group">
            <a href="{{ route('services.create') }}" class="btn btn-primary">Создать</a>
        </div>

        <form class="" method="GET">
            <div class="form-group">
                <label for="service-category">Категория:</label>
                <select name="category_id" id="service-category" class="form-control js-services-index">
                    <option value="0">- Все -</option>
                    @foreach($categories as $category)
                        <option value="{{ $category['id'] }}" @if(request()->get('category_id') == $category['id']) selected @endif>
                            {{ $category['name'] }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="service-subcategory">Подкатегория:</label>
                <select name="service_category_id" id="service-subcategory" class="form-control">
                    <option value="0">- Все -</option>
                    @foreach($service_categories as $category)
                        <option value="{{ $category['id'] }}">
                            {{ $category['name'] }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Отфильтровать">
            </div>
        </form>

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <td class="col-md-2">Название
                    <a href="{{ route('services.index', ['sort_by' => 'name_ru', 'direction' => 'asc']) }}">↑</a>
                    <a href="{{ route('services.index', ['sort_by' => 'name_ru', 'direction' => 'desc']) }}">↓</a>
                </td>
                <td class="col-md-1">Описание
                    <a href="{{ route('services.index', ['sort_by' => 'description', 'direction' => 'asc']) }}">↑</a>
                    <a href="{{ route('services.index', ['sort_by' => 'description', 'direction' => 'desc']) }}">↓</a>
                </td>
                <td class="col-md-1">Категория
                    <a href="{{ route('services.index', ['sort_by' => 'category_id', 'direction' => 'asc']) }}">↑</a>
                    <a href="{{ route('services.index', ['sort_by' => 'category_id', 'direction' => 'desc']) }}">↓</a>
                </td>
                <td class="col-md-1">Тип
                    <a href="{{ route('services.index', ['sort_by' => 'type', 'direction' => 'asc']) }}">↑</a>
                    <a href="{{ route('services.index', ['sort_by' => 'type', 'direction' => 'desc']) }}">↓</a>
                </td>
                <td class="col-md-1">Подкатегория
                    <a href="{{ route('services.index', ['sort_by' => 'service_category_id', 'direction' => 'asc']) }}">↑</a>
                    <a href="{{ route('services.index', ['sort_by' => 'service_category_id', 'direction' => 'desc']) }}">↓</a>
                </td>
                <td class="col-md-2">Создано
                    <a href="{{ route('services.index', ['sort_by' => 'created_at', 'direction' => 'asc']) }}">↑</a>
                    <a href="{{ route('services.index', ['sort_by' => 'created_at', 'direction' => 'desc']) }}">↓</a>
                </td>
                <td class="col-md-1">Редактировать</td>
                <td class="col-md-1">Удалить</td>
            </tr>
            </thead>

            <tbody>
            @foreach($services as $service)
                <tr>
                    <td>{{ $service['name_ru'] }}</td>
                    <td>{{ str_limit($service['description'], 50) }}</td>
                    <td>{{ $service->category['name'] }}</td>
                    <td>{{ $service['type'] }}</td>
                    <td>{{ $service->service_category['name'] }}</td>
                    <td>{{ $service['created_at'] }}</td>
                    <td><a class="btn btn-primary" href="{{ route('services.edit', ['id' => $service['id']]) }}">Редактировать</a></td>
                    <td>
                        <form action="{{ route('services.destroy', ['id' => $service['id']]) }}" method="POST">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <input type="submit" class="btn btn-danger" value="Удалить">
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </section>
@endsection