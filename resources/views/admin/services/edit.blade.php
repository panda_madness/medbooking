@extends('layouts.app')

@section('content')
    <section>

        <h1>Редактировать сервис</h1>

        <form action="{{ route('services.update', ['id' => $service['id']]) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group">
                <label for="service-name">Название</label>
                <input type="text" id="service-name" class="form-control" name="name_ru" value="{{ $service['name_ru'] }}">
            </div>

            <div class="form-group">
                <label for="service-description">Описание</label>
                <textarea id="service-description" class="form-control html-editor" name="description">{{ $service['description'] }}</textarea>
            </div>

            <div class="form-group">
                <label for="service-category">Категория</label>
                <input type="text" id="service-category" class="form-control" value="{{ $service->category['name'] }}" disabled>
            </div>

            <div class="form-group">
                <label for="service-subcategory">Подкатегория</label>
                <input type="text" id="service-subcategory" class="form-control" value="{{ $service->service_category['name'] }}" disabled>
            </div>

            <div class="form-group">
                <h3>Тип</h3>
                <div class="radio">
                    <label>
                        <input type="radio" name="type" value="default" @if($service['type'] == 'default') checked @endif>
                        Default
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="type" value="popular" @if($service['type'] == 'popular') checked @endif>
                        Popular
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="type" value="null" @if($service['type'] == null) checked @endif>
                        None
                    </label>
                </div>
            </div>

            <div class="form-group">
                <h3>Пол</h3>
                <div class="radio">
                    <label>
                        <input type="radio" name="gender" value="male" @if($service['gender'] == 'male') checked @endif>
                        Мужской
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="gender" value="female" @if($service['gender'] == 'female') checked @endif>
                        Женский
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="gender" value="general" @if($service['gender'] == 'general') checked @endif>
                        Общий
                    </label>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>

    </section>
@endsection