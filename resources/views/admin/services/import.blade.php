@extends('layouts.app')

@section('content')
    <h2>Загрузка сервисов</h2>
    <form action="/admin/services/import" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="services">File</label>
            <input type="file" name="services">
        </div>

        <div class="form-group">
            <input class="btn btn-default" type="submit" value="Submit">
        </div>
    </form>

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    @endif

    @if(session('message'))
        <div class="alert alert-success">
            <span>{{ session('message') }}</span>
        </div>
    @endif
@endsection