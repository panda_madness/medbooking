@extends('layouts.app')

@section('content')
    <section>

        <h1>Редактировать сервис</h1>

        <form action="{{ route('services.store') }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="form-group">
                <label for="service-name">Название</label>
                <input type="text" id="service-name" class="form-control" name="name_ru">
            </div>

            <div class="form-group">
                <label for="service-description">Описание</label>
                <textarea id="service-description" class="form-control html-editor" name="description"></textarea>
            </div>

            <div class="form-group">
                <label for="service-category">Категория</label>
                <select name="category_id" id="service-category" class="form-control">
                    @foreach($categories as $category)
                        <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="service-subcategory">Подкатегория</label>
                <select name="service_category_id" id="service-subcategory" class="form-control">
                    @foreach($service_categories as $category)
                        <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <h3>Тип</h3>
                <div class="radio">
                    <label>
                        <input type="radio" name="type" value="default">
                        Default
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="type" value="popular">
                        Popular
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="type" value="null" checked>
                        None
                    </label>
                </div>
            </div>

            <div class="form-group">
                <h3>Пол</h3>

                <div class="radio">
                    <label>
                        <input type="radio" name="gender" value="general" checked>
                        Общий
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="gender" value="male">
                        Мужской
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="gender" value="female">
                        Женский
                    </label>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>

    </section>
@endsection