<nav class="navbar">

    <div class="navbar__container">
        <div class="navbar__wrap navbar__wrap--left">

            <a href="/">
                <img src="/img/logo.svg" alt="">
            </a>

            <ul class="navbar__item navbar__list">
                <li><a href="{{ route('booking.search.bundles') }}">Бронирование</a></li>
                {{--<li><a href="{{ route('cart.index') }}">Корзина</a></li>--}}
            </ul>
        </div>
        <div class="navbar__wrap navbar__wrap--right">
            <ul class="navbar__item navbar__list">
                <li>
                    <a data-open="help-modal">Помогите мне</a>
                </li>
                @if(Auth::check())
                    <li><a href="{{ route('profile.index') }}">Личный кабинет</a></li>
                @else
                    <li><a href="{{ route('register') }}">Регистрация</a></li>
                    <li><a href="{{ route('login') }}">Войти</a></li>
                @endif
            </ul>
        </div>

        <div class="reveal" id="help-modal" data-reveal>
            <h3>Нужна помощь? Напишите нам</h3>
            <form action="{{ route('help-form') }}" method="post">

                {{ csrf_field() }}

                <div class="row">
                    <div class="column medium-12">
                        <label>Имя
                            <input type="text" name="name">
                        </label>
                    </div>

                    <div class="column medium-12">
                        <label>Email
                            <input type="text" name="email">
                        </label>
                    </div>

                    <div class="column medium-12">
                        <label>Сообщение
                            <textarea name="message" id="" cols="30" rows="10"></textarea>
                        </label>
                    </div>

                    <div class="column medium-12">
                        <input type="submit" value="Отправить" class="button">
                    </div>
                </div>
            </form>
        </div>
    </div>
</nav>