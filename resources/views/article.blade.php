@extends('layouts.app')

@section('content')
    <h1>Статья</h1>
    <h2>{{ $article['title'] }}</h2>
    <p>{{ $article['contents'] }}</p>
@endsection