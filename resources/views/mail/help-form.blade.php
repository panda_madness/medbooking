<table>
    <thead>
        <tr>
            <td>Имя</td>
            <td>Email</td>
            <td>Сообщение</td>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{ $name }}</td>
            <td>{{ $email }}</td>
            <td>{{ $user_message }}</td>
        </tr>
    </tbody>
</table>