@extends('layouts.app')

@section('content')

    <h1>Корзина</h1>

    <h2>Бандлы</h2>

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Название</td>
                <td>Удалить</td>
            </tr>
        </thead>

        <tbody>
            @foreach($cart['bundles'] as $bundle)
                <tr>
                    <td>{{ $bundle['name'] }}</td>
                    <td><a href="/cart/delete/bundles/{{ $bundle['id'] }}">Удалить</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <h2>Услуги</h2>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>Название</td>
            <td>Удалить</td>
        </tr>
        </thead>

        <tbody>
        @foreach($cart['services'] as $service)
            <tr>
                <td>{{ $service['name'] }}</td>
                <td><a href="/cart/delete/services/{{ $service['id'] }}">Удалить</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <a href="/cart/send" class="btn btn-default">Отправить заказ</a>

@endsection