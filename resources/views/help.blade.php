@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="column medium-12 text-center">
            <h2>Спасибо за вашу заявку!</h2>
            <p>Мы скоро с вами свяжемся.</p>
        </div>
    </div>
@endsection