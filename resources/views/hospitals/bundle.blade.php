@extends('layouts.app')

@section('content')

    <section class="hospital">
        <div class="row">
            <div class="column medium-8">
                <h3 class="hospital__title">{{ $hospital->name }}</h3>
                <div class="hospital__image">
                    <img class="" src="{{ asset('storage/' . $hospital->image) }}">
                </div>

                <div class="hospital__description">
                    {!! $hospital->description !!}
                </div>
            </div>

            <div class="column medium-4 hospital__bundle">
                <h3 class="bundle__title">{{ $bundle->name }}</h3>

                @if($bundle->description)
                    <div class="bundle__description bundle__section">{!! $bundle->description !!}</div>
                @endif

                @if($bundle->goal)
                    <div class="bundle__goal bundle__section">
                        <h4 class="bundle__heading">Цель:</h4>
                        <span>{!! $bundle->goal !!}</span>
                    </div>
                @endif

                @if($bundle->notes)
                    <div class="bundle__notes bundle__section">
                        <h4 class="bundle__heading">Заметки:</h4>
                        <span>{!! $bundle->notes !!}</span>
                    </div>
                @endif

                @if($bundle->services)
                    <div class="bundle__services bundle__section">
                        <h4 class="bundle__heading">Включает в себя:</h4>
                        <ul>
                        @foreach($bundle->services as $service)
                            <li>{{ $service->name_ru }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif

                <div class="bundle__price bundle__section">
                    <div class="price-item">
                        Цена (муж.): <span>{{ $bundle->price_male }} ₩</span>
                    </div>
                    <div class="price-item">
                        Цена (жен.): <span>{{ $bundle->price_female }} ₩</span>
                    </div>
                </div>

                <div class="bundle__section">
                    <button data-open="reserve-modal" class="button bundle__reserve">Забронировать</button>
                </div>
            </div>
        </div>

        <div id="reserve-modal" class="reveal" data-reveal data-close-on-click="true">
            <form action="{{ route('order.create') }}" method="post" data-ajax-form>

                <h2>Вы действительно хотите забронировать данный комлект?</h2>

                {{ csrf_field() }}
                <input type="hidden" name="bundle_id" value="{{ $bundle->id }}">

                <div data-form-controls>
                    <button type="submit" class="button" data-form-submit>Да</button>
                    <button class="button" data-close>Нет</button>
                </div>

                <div class="hidden" data-success-msg>
                    Ваша заявка отправлена!
                </div>

                <div class="hidden" data-fail-msg>
                    Что-то пошло не так. Попробуйте повторить попытку позже.
                </div>
            </form>

            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </section>

@endsection