@extends('layouts.app')

@section('content')

    <h1>Новости</h1>

    @foreach($news as $item)
        <h2>{{ $item['title'] }}</h2>
        <p>{{ $item['contents'] }}</p>
    @endforeach

@endsection
