@extends('layouts.app')

@section('content')

    <section class="front-hero text-center">
        <form method="get" action="{{ route('booking.search.bundles') }}">
            <h2 class="front-hero__title">Онлайн бронирование медицинских услуг <br> по всей Южной Корее</h2>

            <div class="row align-center">

                <div class="column medium-7 front-hero__categories">
                    @foreach($categories as $item)
                        <div class="categories__item">

                            <input type="radio" name="bundle_category" id="category-{{ $loop->index }}"
                                   value="{{ $item->id }}" {{ $loop->first ? 'checked':'' }}>

                            <div class="categories__item-icon">
                                <img class="icon" src="/img/complex.svg" alt="">
                                <img class="icon icon--active" src="/img/complex-white.svg" alt="">
                            </div>

                            <span class="categories__item-bg"></span>

                            <span class="categories__item-title">{{ $item->name }}</span>

                            <label for="category-{{ $loop->index }}"></label>
                        </div>
                    @endforeach
                </div>

            </div>

            <div class="row align-center">
                <div class="column medium-5">
                    <div class="front-hero__inputs">

                        <div class="form-group">
                            <input type="text" name="bundle_name" placeholder="Название комплекта" class="form-control">
                        </div>

                        <button type="submit" class="button inputs__submit">Поиск</button>
                    </div>
                </div>
            </div>
        </form>
    </section>

    <section class="front-about">
        <div class="row align-center">
            <div class="column medium-4">
                <img class="about__logo" src="/img/logo-big.svg" alt="">
            </div>
        </div>

        <div class="row align-center">
            <div class="column medium-6">
                <p class="about__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut deleniti distinctio ducimus illum incidunt minus omnis placeat porro quis, rem sed veritatis voluptates? A doloremque eos expedita maxime quibusdam quos reiciendis suscipit velit! Commodi deserunt maiores repellendus sint. Adipisci distinctio dolore earum in nam rem sequi suscipit veritatis voluptate!</p>
            </div>
        </div>

        <a href="/" class="button button--trans about__link">Узнать больше</a>
    </section>

    <section class="front-register">
        <div class="row register__wrapper align-center">
            <div class="column medium-6">
                <h3 class="register__title">Зарегистрируйтесь, чтобы получать особые предложения!</h3>
                <a href="/" class="register__link button">Регистрация</a>
            </div>
        </div>
    </section>

@endsection
