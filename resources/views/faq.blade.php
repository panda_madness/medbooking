@extends('layouts.app')

@section('content')
    @foreach($faqs as $item)
        <h3>Вопрос: {{ $item['question'] }}</h3>
        <span>Ответ: {{ $item['answer'] }}</span>
    @endforeach
@endsection