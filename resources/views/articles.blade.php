@extends('layouts.app')

@section('content')
    <h1>Статьи</h1>
    @foreach($articles as $item)
        <h2><a href="/articles/{{ $item['id'] }}">{{ $item['title'] }}</a></h2>
    @endforeach
@endsection