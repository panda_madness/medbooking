@extends('layouts.app')

@section('content')

@foreach($hospitals as $hospital)
    <div class="panel panel-default">
        <div class="panel-heading">{{ $hospital->name }}</div>
        <div class="panel-body">
            <h3>Услуги</h3>
            <ul class="list-group">
                @foreach($hospital->services as $service)
                    <li class="list-group-item">
                        {{ $service->name_ru }}: {{ $service->pivot->price }}
                    </li>
                @endforeach
            </ul>

            <h3>Бандлы</h3>
            <ul class="list-group">
                @foreach($hospital->bundles as $bundle)
                    <li class="list-group-item">
                        {{ $bundle->name }}: Ж {{ $bundle->price_female }}, М {{ $bundle->price_male }}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endforeach

@endsection