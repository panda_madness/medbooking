@extends('layouts.app')

@section('content')

    <h2>Бандл: {{ $bundle['name'] }}</h2>

    <p>{{ $bundle['description'] }}</p>

    <h3>Сервисы в этом бандле</h3>

    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <td>Название</td>
            </tr>
        </thead>
        <tbody>
        @foreach($bundle->services as $service)
            <tr>
                <td>{{ $service['name'] }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <a href="/cart/add/bundles/{{ $bundle['id'] }}" class="btn btn-default">Добавить в корзину</a>

@endsection