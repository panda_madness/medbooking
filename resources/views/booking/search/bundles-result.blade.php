@extends('layouts.app')

@section('content')

    <section class="booking">
        <div class="row">
            <div class="column medium-3 booking__filters">
                <form action="{{ route('booking.search.bundles') }}" method="get">
                    <!-- <div class="filters__category">
                        <div class="filters__title">Категория поиска</div>

                        <ul class="filters__list">
                            @foreach($categories as $item)
                                <li>
                                    <input type="radio" name="bundle_category" title="Категория" value="{{ $item->id }}"
                                           id="category-{{ $item->id }}"
                                            {{ request()->get('bundle_category') == $item->id ? 'checked':'' }}
                                            {{ $loop->first ? 'checked':'' }}

                                    >
                                    <label for="category-{{ $item->id }}">{{ $item->name }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div> -->

                    <div class="filters__category">
                        <div class="filters__title">Класс</div>

                        <ul class="filters__list">
                            @foreach($classes as $item)
                                <li>
                                    <input type="radio" name="bundle_class" title="Класс" value="{{ $item['name'] }}"
                                           id="class-{{ $item['id'] }}"
                                            {{ request()->get('bundle_class') == $item['name'] ? 'checked':'' }}
                                            {{ $loop->first ? 'checked':'' }}

                                    >
                                    <label for="class-{{ $item['id'] }}">{{ $item['title'] }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="filters__name">
                        <div class="filters__title">Название</div>

                        <input type="text" name="bundle_name" title="Название"
                               value="{{ request()->get('bundle_name') }}">
                    </div>

                    <div class="filters__price">
                        <div class="filters__title">Цена</div>

                        <div class="slider" data-slider data-initial-start="{{ request()->get('price_from') ?? 0 }}" data-initial-end="{{ request()->get('price_to') ?? 10000000 }}"
                             data-start="0" data-end="10000000" data-step="10000">
                            <span class="slider-handle" data-slider-handle role="slider" tabindex="1"
                                  aria-controls="price_from"></span>
                            <span class="slider-fill" data-slider-fill></span>
                            <span class="slider-handle" data-slider-handle role="slider" tabindex="1"
                                  aria-controls="price_to"></span>
                        </div>

                        <label>Цена от:
                            <input type="number" name="price_from" id="price_from">
                        </label>

                        <label>Цена до:
                            <input type="number" name="price_to" id="price_to">
                        </label>
                    </div>

                    <div class="filters__submit">
                        <input class="button filters__submit" type="submit" value="Поиск">
                    </div>
                </form>
            </div>
            <div class="column medium-9 booking__results">

                <div class="row expanded results__header">
                    <div class="column medium-6">
                        <h2 class="header__title">{{ $categories->where('id', request()->get('bundle_category', $categories->first()->id))->first()->name }}</h2>
                    </div>

                    <div class="column medium-6 header__content">
                        Найдено {{ $bundles->isNotEmpty() ? $bundles->total() : 0 }} результата
                    </div>
                </div>

                @if($bundles->isNotEmpty())
                    @foreach($bundles->chunk(3) as $chunk)
                        <div class="row expanded">
                            @foreach($chunk as $bundle)
                                <div class="column medium-4">
                                    <div class="results__item">
                                        <img class="item__image" src="{{ !empty($bundle->hospital->image) ? asset('storage/' . $bundle->hospital->image) : '/img/no-image.gif' }}"
                                             alt="">

                                        <div class="item__content">
                                            <div class="item__title">{{ $bundle->name }}</div>
                                            <div class="item__subtitle">{{ $bundle->hospital->name }}</div>
                                            <div class="item__price">
                                                <div class="item__price-text">Цена (муж.):
                                                    <span class="item__price-text item__price-text--blue">{{ $bundle->price_male ?? 'Не для мужчин' }} ₩</span>
                                                </div>
                                                <div class="item__price-text">Цена (жен.):
                                                    <span class="item__price-text item__price-text--blue">{{ $bundle->price_female ?? 'Не для женщин' }} ₩</span>
                                                </div>
                                            </div>

                                            <a href="{{ route('hospitals.bundle', ['hospital' => $bundle->hospital->id, 'bundle' => $bundle->id]) }}" class="item__link button">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach

                    {{ $bundles->appends(request()->all())->links() }}
                @else
                    Ничего не найдено!
                @endif
            </div>
        </div>
    </section>

    {{--@foreach($hospitals as $hospital)--}}
        {{--<div class="panel panel-default">--}}
            {{--<div class="panel-heading">--}}
                {{--<h3 class="panel-title">{{ $hospital->name }}</h3>--}}
            {{--</div>--}}
            {{--<div class="panel-body">--}}
                {{--<ul class="list-group">--}}
                    {{--@foreach($hospital->bundles as $bundle)--}}
                        {{--<li class="list-group-item">--}}
                            {{--<a href="">{{ $bundle->name }}</a>--}}
                        {{--</li>--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--@endforeach--}}
@endsection