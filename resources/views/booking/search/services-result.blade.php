@extends('layouts.app')

@section('content')
    @foreach($hospitals as $hospital)
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ $hospital->name }}</h3>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                    @foreach($hospital->services as $service)
                        <li class="list-group-item">
                            <a href="">{{ $service->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endforeach
@endsection