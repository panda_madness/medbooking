@extends('layouts.app')

@section('content')

    <h2>Услуга: {{ $service['name'] }}</h2>

    <p>{{ $service['description'] }}</p>

    <a href="/cart/add/services/{{ $service['id'] }}" class="btn btn-default">Добавить в корзину</a>

@endsection