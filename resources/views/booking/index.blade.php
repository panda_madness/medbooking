@extends('layouts.app')

@section('content')
    <h2>Поиск</h2>

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#bundles" aria-controls="bundles" role="tab" data-toggle="tab">Комплексные обследования</a>
        </li>

        <li role="presentation">
            <a href="#surgery" aria-controls="surgery" role="tab" data-toggle="tab">Хирургия</a>
        </li>

        <li role="presentation">
            <a href="#plastic-surgery" aria-controls="plastic-surgery" role="tab" data-toggle="tab">Пластическая хирургия</a>
        </li>

        <li role="presentation">
            <a href="#eastern" aria-controls="settings" role="tab" data-toggle="tab">Восточная медицина</a>
        </li>

        <li role="presentation">
            <a href="#wellness" aria-controls="settings" role="tab" data-toggle="tab">Веллнес центры</a>
        </li>

        <li role="presentation">
            <a href="#dentistry" aria-controls="settings" role="tab" data-toggle="tab">Стоматология</a>
        </li>

        <li role="presentation">
            <a href="#diagnostics" aria-controls="settings" role="tab" data-toggle="tab">Диагностика, лечение</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="bundles">
                    <form action="{{ route('booking.search.bundles') }}" method="GET">
                        <div class="form-group">
                            <label for="bundle_city">Город</label>
                            <select name="bundle_city" id="bundle_city" class="form-control"
                                    data-binded-select="#bundle_hospital"
                                    data-url="{{ route('hospitals.get-by-city') }}"
                                    data-value-key="id"
                                    data-contents-key="name"
                                    data-ajax-value-key="id">
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="bundle_hospital">Госпиталь</label>
                            <select name="bundle_hospital" id="bundle_hospital" class="form-control">
                                <option value="" data-default-option>-- Все --</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="bundle_name">Название</label>
                            <input type="text" name="bundle_name" id="bundle_name" class="form-control">
                        </div>

                        <input type="submit" class="btn btn-primary" value="Поиск">
                    </form>
                </div>

                <div role="tabpanel" class="tab-pane" id="surgery">
                    <form action="{{ route('booking.search.services') }}" method="GET">

                        <input type="hidden" name="category" value="{{ $surgery_category->id }}">

                        <div class="form-group">
                            <label for="s_service_category">Категория</label>
                            <select name="service_category" id="s_service_category" class="form-control"
                                    data-binded-select="#s_service"
                                    data-url="{{ route('services.get-by-service-category') }}"
                                    data-value-key="id"
                                    data-contents-key="name_ru"
                                    data-ajax-value-key="id">
                                @foreach($surgery_category->service_categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="s_service">Услуга</label>
                            <select name="service" id="s_service" class="form-control"></select>
                        </div>

                        <input type="submit" class="btn btn-primary" value="Поиск">
                    </form>
                </div>

                <div role="tabpanel" class="tab-pane" id="plastic-surgery">
                    <form action="{{ route('booking.search.services') }}" method="GET">

                        <input type="hidden" name="category" value="{{ $plastic_surgery->id }}">

                        <div class="form-group">
                            <label for="ps_service_category">Категория</label>
                            <select name="service_category" id="ps_service_category" class="form-control"
                                    data-binded-select="#ps_service"
                                    data-url="{{ route('services.get-by-service-category') }}"
                                    data-value-key="id"
                                    data-contents-key="name_ru"
                                    data-ajax-value-key="id">
                                @foreach($plastic_surgery->service_categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="ps_service">Услуга</label>
                            <select name="service" id="ps_service" class="form-control"></select>
                        </div>

                        <input type="submit" class="btn btn-primary" value="Поиск">
                    </form>
                </div>

                <div role="tabpanel" class="tab-pane" id="eastern">

                </div>

                <div role="tabpanel" class="tab-pane" id="wellness">

                </div>

                <div role="tabpanel" class="tab-pane" id="dentistry">
                    <form action="{{ route('booking.search.services') }}" method="GET">

                        <input type="hidden" name="category" value="{{ $dentistry->id }}">

                        <div class="form-group">
                            <label for="d_service_category">Категория</label>
                            <select name="service_category" id="d_service_category" class="form-control"
                                    data-binded-select="#d_service"
                                    data-url="{{ route('services.get-by-service-category') }}"
                                    data-value-key="id"
                                    data-contents-key="name_ru"
                                    data-ajax-value-key="id">
                                @foreach($dentistry->service_categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="d_service">Услуга</label>
                            <select name="service" id="d_service" class="form-control"></select>
                        </div>

                        <input type="submit" class="btn btn-primary" value="Поиск">
                    </form>
                </div>

                <div role="tabpanel" class="tab-pane" id="diagnostics">

                </div>
            </div>
        </div>
    </div>
@endsection