$('#service-category').on('change', function() {

    let category_id = $(this).val();

    $('#service-subcategory').load('/admin/services/get-service-categories?' + $.param({
        category_id: category_id
    }), () => {
        if($(this).hasClass('js-services-index')) {
            $('#service-subcategory').prepend('<option value="0">- Все -</option>');
        }
    });
});