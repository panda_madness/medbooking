$('#hospital-edit').on('submit', function (e) {

    // TODO: Redo this mechanism
    let $services = $('.service-data');
    let service_data = [];

    $services.each(function(index) {
        if($(this).val() !== '') {
            service_data.push({
                id: $(this).attr('data-service-id'),
                price: $(this).val()
            });
        }
    });

    service_data.forEach((el) => {
        $(this).append(`<input type="hidden" name="services[]" value="${el.id}:${el.price}">`);
    });

    return true;
});