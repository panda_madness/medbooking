$(document).ready(() => {

    let loadSelectContents = function (target) {

        let $subject = {};

        if(target instanceof HTMLElement) {
            $subject = $(target);
        } else {
            $subject = $(target.target);
        }
        let $target = $($subject.attr('data-binded-select'));
        let url = $subject.attr('data-url');
        let contents = $target.children('option[data-default-option]');
        let optionValueKey = $subject.attr('data-value-key');
        let optionContentsKey = $subject.attr('data-contents-key');
        let ajaxValueKey = $subject.attr('data-ajax-value-key');
        let data = {};

        data[ajaxValueKey] = $subject.val();

        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'json',
            data: data,
            success: (res) => {
                $target.html('');
                $target.append(contents);
                if(Array.isArray(res)) {
                    res.forEach((el) => {
                        $target.append(`<option value="${el[optionValueKey]}">${el[optionContentsKey]}</option>`);
                    });
                }
            },
            error: () => {}
        });
    };

    let $bindedSelects = $('select[data-binded-select]').each((index, el) => {
        loadSelectContents(el);
    });

    $bindedSelects.on('change', loadSelectContents);
});