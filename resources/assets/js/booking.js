$(document).ready(() => {
    $('[data-ajax-form]').on('submit', (e) => {
        e.preventDefault();

        let $form = $(e.currentTarget);

        let $buttons = $form.find('input[type="button"], input[type="submit"], button')

        $buttons.on('click', (e) => {
            e.preventDefault();
        });

        let data = {};

        $($form.serializeArray()).each((index, obj) => {
            data[obj.name] = obj.value;
        });

        let url = $form.attr('action');

        $form.find('[data-form-submit]').on('click', () => {

            $.ajax({
                url: url,
                data: data,
                dataType: 'json',
                method: 'POST',
                success: (res) => {
                    $form.find('[data-success-msg]').show();
                    $form.find('[data-form-controls]').hide();
                },
                error: (res) => {
                    $form.find('[data-fail-msg]').show();
                    $form.find('[data-form-controls]').hide();
                }
            });
        })
    });
});